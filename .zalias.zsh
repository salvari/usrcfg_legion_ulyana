# alias svim='vim -u ~/.SpaceVim/vimrc'

# list alias
alias lal='alias |grep '

# make directory and cd to it
amd() {
    mkdir -p $1
    cd $1
}

# marge pdf files with ghostscript
pdfmerge() {
    gs -dBATCH -dNOPAUSE -q -sPAPERSIZE=a4 -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=$@ ;
}

# dockerhublist
dockerhublist() {
    wget -q https://registry.hub.docker.com/v1/repositories/$1/tags -O - | \
    sed -e 's/[][]//g' -e 's/"//g' -e 's/ //g' | \
    tr '}' '\n'  | \
    awk -F: '{print $3}'
}

# tempo
# tempo as_pontes
# tempo coruña
tempo(){
    curl wttr.in/${1}
}

# noise generators (you need sox)
alias bnoise='play -c 2 -n synth brownnoise band -n 2500 4000 reverb 20'
alias pnoise='play -c 2 -n synth pinknoise band -n 2500 4000 reverb 20'
alias wnoise='play -c 2 -n synth whitenoise band -n 2500 4000 reverb 20'

# Backups commands
alias bkpbooks='rsync -az --delete /store/salvari/Biblioteca salvari@192.168.0.130:/mnt/usbA/Backup/salvari/legion'

# systemd aliases
# Base
alias sc=systemctl
alias scu='systemctl --user'

# systemd subcommands
alias scst='sudo systemctl start'
alias scsp='sudo systemctl stop'
alias scrl='sudo systemctl reload'
alias scrt='sudo systemctl restart'
alias sce='sudo systemctl enable'
alias scd='sudo systemctl disable'

alias scs='systemctl status'
alias scsw='systemctl show'
alias sclu='systemctl list-units'
alias scluf='systemctl list-unit-files'
alias sclt='systemctl list-timers'
alias scc='systemctl cat'
alias scie='systemctl is-enabled'

# emacs aliases
alias ef='/home/salvari/.antigen/bundles/robbyrussell/oh-my-zsh/plugins/emacs/emacsclient.sh --create-frame --no-wait'
alias gemacs='/usr/bin/emacs'
embkp(){
    local my_date=`date +"%Y%m%d"`
    if [[ -z $1 ]]
    then
        echo "Unknow qualifier"
        tar cjf ~/apps/emacs/dotemacsd_bkp/${my_date}_unk.tbz ~/.emacs.d
    else
        tar cjf ~/apps/emacs/dotemacsd_bkp/${my_date}_$1.tbz ~/.emacs.d
    fi
}

# golang aliases
alias gob='go build'        # Build your code
alias goc='go clean'        # Removes object files from package source directories
alias god='go doc'          # Prints documentation comments
alias gof='go fmt'          # Gofmt formats (aligns and indents) Go programs.
alias gofa='go fmt ./...'   # Run go fmt for all packages in current directory, recursively
alias gog='go get'          # Downloads packages and then installs them to $GOPATH
alias goi='go install'      # Compiles and installs packages to $GOPATH
alias gol='go list'         # Lists Go packages
alias gom='go mod'          # Access to operations on modules
alias gop='cd $GOPATH'      # Takes you to $GOPATH
alias gopb='cd $GOPATH/bin' # Takes you to $GOPATH/bin
alias gops='cd $GOPATH/src' # Takes you to $GOPATH/src
alias gor='go run'          # Compiles and runs your code
alias got='go test'         # Runs tests
alias gov='go vet'          # Vet examines Go source code and reports suspicious constructs


# esp-idf enviroment
alias myidf='. ~/apps/esp/esp-idf/export.sh'

# platformio CLI virtualenv
alias mypio='source ~/.platformio/penv/bin/activate'

# Python related aliases
# virtualenvwrapper activation
# alias get_vw='source /usr/share/virtualenvwrapper/virtualenvwrapper.sh'
alias myvw='source `pyenv which virtualenvwrapper.sh`'
myve(){
    pip install --upgrade pip setuptools wheel pipx virtualenv virtualenvwrapper
    pip install 'python-lsp-server[all]'
}

# utils
alias ipconfig='ip -c --brief addr show'
alias sl='sudo tail -f /var/log/syslog'
alias hsrv='hugo -D server --disableFastRender'
alias aak='sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv '
alias ls=lsd

# mounts

alias msdock='sshfs dockadmin@siriodock:/home/dockadmin ~/mnt/siriodock'
alias msbkp='sshfs salvari@sirio:/mnt/usbA/Backup ~/mnt/sirio'

# radio
alias rne1='vlc -I ncurses https://crtve--di--crtve-ice--01--cdn.cast.addradio.de/crtve/rne1/main/aac/high'
alias rnerc='vlc -I ncurses https://crtve--di--crtve-ice--01--cdn.cast.addradio.de/crtve/rnerc/main/aac/high'
alias rner3='vlc -I ncurses https://crtve--di--crtve-ice--01--cdn.cast.addradio.de/crtve/rner3/main/aac/high'
alias rne5='vlc -I ncurses https://crtve--di--crtve-ice--01--cdn.cast.addradio.de/crtve/rne5/main/aac/high'
alias cuacfm='vlc -I ncurses https://streaming.cuacfm.org/cuacfm-128k.mp3'
alias sputnik='vlc -I ncurses http://sputnikradio.es:8009/live'

# echoes of bluemars, the lost fleet
alias bluemars='vlc -I ncurses http://streams.echoesofbluemars.org:8000/bluemars.m3u'
alias cryosleep='vlc -I ncurses http://streams.echoesofbluemars.org:8000/cryosleep.m3u'
alias voices='vlc -I ncurses http://streams.echoesofbluemars.org:8000/voicesfromwithin.m3u'

#disks
alias disks='echo ${green}"╓───── m o u n t . p o i n t s"${rst}; echo ${green}"╙────────────────────────────────────── ─ ─ "${rst}; lsblk -a; echo ""; echo ${yellow}"╓───── d i s k . u s a g e"; echo "╙────────────────────────────────────── ─ ─ "${rst}; df -h; echo ""; echo ${purple}"╓───── U.U.I.D.s"; echo "╙────────────────────────────────────── ─ ─ "${rst}; lsblk -f;'

#Variables
PUBLIC_IP=`wget http://ipecho.net/plain -O - -q ; echo`

# Colores
bold=$(tput bold)               # Bold
red=$bold$(tput setaf 1)        # Red
green=${bold}$(tput setaf 2) 	# Green
yellow=${bold}$(tput setaf 3) 	# Yellow
blue=${bold}$(tput setaf 4) 	# Blue
purple=${bold}$(tput setaf 5) 	# Purple
cyan=${bold}$(tput setaf 6) 	# Cyan
gray=$(tput setaf 7)            # dim white text
white=${bold}$(tput setaf 7) 	# White
rst=$(tput sgr0) 		        # Text reset
