[[ -e ~/.profile ]] && emulate sh -c 'source ~/.profile'
source ~/apps/zsh-git-prompt/zshrc.sh
source ~/apps/antigen/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

antigen bundles <<EOFBUNDLES
  # Bundles from the default repo (robbyrussell's oh-my-zsh)
  git
  command-not-found
  tmux
  # extracts every kind of compressed file
  extract
  # jump to dir used frequently, switched from z to fasd
  # needs =apt install fasd=
  # z
  fasd
  # common
  common-aliases
  # virtualenvwrapper (not compatible with pyenv)
  # robbyrussell/oh-my-zsh plugins/virtualenvwrapper
  # pyenv
  mattberther/zsh-pyenv
  # commandline autocompletions
  zsh-users/zsh-completions
  # emacs shortcuts and helpers
  emacs
  # cmdline completion for docker
  docker
  # cmdline completion for docker compose
  docker-compose
  # cmdline completion for docker
  # docker-machine
  # Learn alias
  MichaelAquilina/zsh-you-should-use
  zsh-users/zsh-history-substring-search
  # autosuggestions
  tarruda/zsh-autosuggestions
  # Syntax highlighting
  zsh-users/zsh-syntax-highlighting
EOFBUNDLES

#antigen theme agnoster
antigen theme gnzh

# Tell antigen that you're done.
antigen apply

# Correct rm alias from common-alias bundle
unalias rm
alias rmi='rm -i'

# Read alias from file
source ~/.zalias.zsh

# pyenv activation is done by pyenv plugin but this enables
# pyenv completion
if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
    # export PYENV_SHELL=zsh
    # source '/home/salvari/.pyenv/libexec/../completions/pyenv.zsh'
    # command pyenv rehash 2>/dev/null
fi
