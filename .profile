# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# adds texlive to my PATH
if [ -d "/opt/texbin" ] ; then
    PATH="$PATH:/opt/texbin"
fi

#--------------------
# python
# WORKON_HOME for virtualenvwrapper
if [ -d "$HOME/.virtualenvs" ] ; then
    export WORKON_HOME="$HOME/.virtualenvs"
fi

# pyenv
if [ -d "$HOME/.pyenv" ] ; then
    export PYENV_ROOT="$HOME/.pyenv"
    # export PATH="$PYENV_ROOT/bin:$PATH"
    # eval "$(pyenv init --path)"
fi

# poetry
if [ -d "$HOME/.poetry" ] ; then
    PATH="$HOME/.poetry/bin:$PATH"
fi

#--------------------
# cargo for rust
if [ -d "$HOME/.cargo/bin" ] ; then
    export PATH="$PATH:$HOME/.cargo/bin"
fi

#--------------------
# nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#--------------------
#golang
if [ -d "/usr/local/go" ] ; then
#     export GOROOT="/usr/local/go"
    export PATH="$PATH:/usr/local/go/bin"
fi
# if [ -d "$HOME/work/development/gocode" ] ; then
#     export GOPATH="$HOME/work/development/gocode"
#     export GOBIN="$GOPATH/bin"
#     PATH="$PATH:$GOPATH/bin"
# fi
if [ -d "${HOME}/go" ] ; then
    export GOPATH="${HOME}/go"
#    export GOBIN="$GOPATH/bin"
    PATH="$PATH:$GOPATH/bin"
fi


export PATH
